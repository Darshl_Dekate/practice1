//problem1. Find all Web Developers

const givenArray = require("./index.js")


let res = givenArray.filter((Array) => {
    if (Array["job"].match(/Web Developer/)) {
        return Array
    }
});
console.log(res);




// problem 2 .Convert all the salary values into proper numbers instead of strings
let res1 = givenArray.map((Array) => {
    let number = Array["salary"].substring(1);
    return Number(number);

});
console.log(res1);

// problem 4
let intialValue = 0;
let res3 = givenArray.reduce((acc, currentValue) => {
    let number = Number(currentValue["salary"].replace("$", ""))
    return acc + number
}, 0);

console.log(res3)

//5. Find the sum of all salaries based on country using only HOF method
let res4 = givenArray.reduce((previousValue, currentValue) => {
    if (!previousValue[currentValue['location']]) {
        previousValue[currentValue['location']] = Number(currentValue["salary"].replace("$", ""));
    } else {
        previousValue[currentValue['location']] += Number(currentValue["salary"].replace("$", ""));
    }
    return previousValue;
}, {});

console.log(res4);